This is a part of Master's Thesis by Mikko Hemilä

The goal was to study how experiments can be implement
with code-level software variability techniques in the context of 
continuous experimentation. 

This is a simplified version of gnome-clock: https://gitlab.gnome.org/GNOME/gnome-clocks/.

Three test experiments were conducted and implemented with three different
techniques: conditional execution, inheritance and aggregation/delegation.

This work is organized into branches.
The master branch is the
base for all the other branches. Each test has its base branch: 
TEST\_1, TEST\_2, and TEST\_3.
Then each implementation has its branch for each of the tests
named by [IMPLEMENTATION]\_[NRO], for example, COND\_EXE\_1, AGGREGATION\_1 or INHERIT\_1
respectively for conditional execution, aggregation/delegation, and inheritance.
The branches each have two commits on top of the test base branch, one for implementing
the test and one for the clean-up.

