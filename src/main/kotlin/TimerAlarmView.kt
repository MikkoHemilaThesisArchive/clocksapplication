import javax.swing.*

class TimerAlarmView : JPanel() {
    private val label = JLabel("0 s")
    private var seconds = 0L
    private val timers = mutableListOf<TimerAlarmItem>()
    private val timerContainer = JPanel()

    init {
        label.horizontalAlignment = SwingConstants.CENTER
        val addMinuteBtn = JButton("+60 s")
        val startBtn = JButton("Start")
        addMinuteBtn.addActionListener {
            seconds += 60
            label.text = "$seconds s"
        }
        startBtn.addActionListener {
            createTimer()
            seconds = 0
            label.text = "0 s"
        }

        val container = JPanel()
        container.add(label)
        container.add(addMinuteBtn)
        container.add(startBtn)

        timerContainer.layout = BoxLayout(timerContainer, BoxLayout.Y_AXIS)
        timerContainer.alignmentY = TOP_ALIGNMENT

        add(container)
        add(timerContainer)

        layout = BoxLayout(this, BoxLayout.Y_AXIS)
    }

    private fun createTimer() {
        val timerAlarmItem = TimerAlarmItem(seconds) { deleteTimer(it) }
        timers.add(timerAlarmItem)
        timerContainer.removeAll()
        for (item in timers) {
            timerContainer.add(item)
        }
        timerContainer.repaint()
    }

    private fun deleteTimer(timerItem: TimerAlarmItem) {
        timers.remove(timerItem)
        timerContainer.removeAll()
        for (item in timers) {
            timerContainer.add(item)
        }
        timerContainer.repaint()
    }
}