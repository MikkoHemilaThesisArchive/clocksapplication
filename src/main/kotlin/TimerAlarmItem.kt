import javax.swing.JButton
import javax.swing.JLabel
import javax.swing.JPanel

class TimerAlarmItem(time: Long, delete: (TimerAlarmItem) -> Unit) : JPanel() {
    private val label = JLabel("$time s")
    private val timer = TimerAlarm(time * 1000) { label.text = "${it / 1000} s"}

    init {
        val cancelBtn = JButton("Cancel")
        cancelBtn.addActionListener {
            timer.cancel()
            delete(this)
        }

        add(label)
        add(cancelBtn)
    }
}