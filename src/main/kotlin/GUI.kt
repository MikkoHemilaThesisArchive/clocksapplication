
import java.awt.CardLayout
import javax.swing.BoxLayout
import javax.swing.BoxLayout.Y_AXIS
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JPanel

class GUI(title: String) : JFrame() {
    init {
        setTitle(title)

        val mainView = JPanel()
        val mainViewLayout = CardLayout()
        mainView.layout = mainViewLayout
        mainView.add("stopwatch", StopwatchView())
        mainView.add("timer", TimerAlarmView())

        val stopwatchBtn = JButton("Stopwatch")
        val timerAlarmBtn = JButton("Timer")
        stopwatchBtn.addActionListener { mainViewLayout.show(mainView, "stopwatch") }
        timerAlarmBtn.addActionListener { mainViewLayout.show(mainView, "timer") }
        val buttons = JPanel()
        buttons.add(stopwatchBtn)
        buttons.add(timerAlarmBtn)

        contentPane.add(buttons)
        contentPane.add(mainView)
        contentPane.layout = BoxLayout(contentPane, Y_AXIS)

        defaultCloseOperation = EXIT_ON_CLOSE
        setSize(300, 200)
        setLocationRelativeTo(null)
    }
}