
import java.time.Instant
import java.util.*
import kotlin.concurrent.timer

class Stopwatch(val updater: (Long) -> Unit) {
    private var time = 0L
    private var timer: Pair<Long, Timer>? = null

    fun start() {
        if (timer == null) {
            timer = Pair(Instant.now().toEpochMilli(), createUpdater())
        }
    }

    fun pause() {
        if (timer?.second != null) {
            time += timer?.let { Instant.now().toEpochMilli() - it.first } ?: 0
            timer?.second?.cancel()
            timer = null
        }
    }

    fun reset() {
        time = 0
        timer?.second?.cancel()
        timer = null
        updater(0)
    }

    private fun getTime(): Long {
        return time + (timer?.let { Instant.now().toEpochMilli() - it.first } ?: 0)
    }

    private fun createUpdater() = timer("stopwatch", daemon = true, period = 1) {
        updater(getTime())
    }

}