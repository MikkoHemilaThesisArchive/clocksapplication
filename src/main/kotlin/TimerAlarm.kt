
import java.time.Instant
import java.util.*
import kotlin.concurrent.schedule
import kotlin.concurrent.timer

class TimerAlarm(time: Long, val update: (Long) -> Unit) {
    private val end = Instant.now().plusMillis(time).toEpochMilli()
    private val updater: Timer = createUpdater()
    private val timer = Timer().schedule(time) {
        updater.cancel()
        println("TIME")
    }

    fun cancel() {
        updater.cancel()
        timer.cancel()
    }

    private fun createUpdater() = timer("stopwatch", daemon = true, period = 1) {
        update(end - Instant.now().toEpochMilli())
    }
}