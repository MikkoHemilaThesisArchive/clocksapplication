import javax.swing.*

class StopwatchView : JPanel() {
    private val label = JLabel("0 ms")
    private val stopwatch = Stopwatch { label.text = "$it ms" }

    init {
        label.horizontalAlignment = SwingConstants.CENTER
        val startBtn = JButton("Start")
        val pauseBtn = JButton("Pause")
        val resetBtn = JButton("Reset")
        startBtn.addActionListener { stopwatch.start() }
        pauseBtn.addActionListener { stopwatch.pause() }
        resetBtn.addActionListener { stopwatch.reset() }
        val buttons = JPanel()
        buttons.add(startBtn)
        buttons.add(pauseBtn)
        buttons.add(resetBtn)

        add(label)
        add(buttons)
        layout = BoxLayout(this, BoxLayout.Y_AXIS)
    }
}